﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EGSoft.Fc.EngineModel;
using EGSoft.Fc.SubspaceModel;
using EGSoft.Fc.SubspaceModel.Modules;

namespace EGSoft.BotModel
{

    public class EGBot : IEGBot
    {
        private IZone zone;
        public IZone Zone
        {
            get { return zone; }
        }

        private ILogin login;
        public ILogin Login
        {
            get { return login; }
        }

        private IArena arena;
        public IArena Arena
        {
            get { return arena; }
        }

        private IMessenger messenger;
        public IMessenger Messenger
        {
            get { return messenger; }
        }


        private IFcEngine engine;

        private String chatList;
        public String ChatList
        {
            get { return chatList; }
            set { chatList = value; }
        }

        public EGBot()
        {
            engine = new FcEngine();
            zone = engine.AddModule<IZone>(new Zone());
            login = engine.AddModule<ILogin>(new Login());
            arena = engine.AddModule<IArena>(new Arena());
            messenger = engine.AddModule<IMessenger>(new Messenger());

            login.LoginResponse += new LoginResponseHandle(login_LoginResponse);

        }

        void login_LoginResponse(object sender, LoginResponseType loginResponseType)
        {
            if (loginResponseType == LoginResponseType.Success)
            {
                arena.Go(ShipType.Spectate, arena.Name);
                messenger.JoinChatChannel(chatList);
            }
        }

        public void Start()
        {
            engine.LoadAllUnloadedModules();
            engine.Start();
            zone.Connect();
        }

        public void Stop()
        {
            zone.Disconnect();
            engine.Stop();
        }

        public IFcEngineModule AddModule<ModuleType>(ModuleType module)
            where ModuleType : IFcEngineModule
        {
            return engine.AddModule<ModuleType>(module);
        }
    }
}
