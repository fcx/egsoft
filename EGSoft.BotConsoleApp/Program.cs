﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EGSoft.Fc.EngineModel;
using EGSoft.Fc.SubspaceModel;
using EGSoft.Fc.SubspaceModel.Modules;
using EGSoft.BotModel;

using EGSoft.Examples.HelloWorldModel.Modules;

using EGSoft.BotConsoleApp.Configuration;

namespace EGSoft.BotConsoleApp
{
    public class Program
    {
        private static readonly BotConsoleAppConfig config = BotConsoleAppConfig.GetConfig();

        static void Main(string[] args)
        {
            EGBot egBot = new EGBot();
            egBot.Zone.IPAddress = config.ZoneIP;
            egBot.Zone.Port = config.ZonePort;
            egBot.Arena.Name = config.DefaultArena;
            egBot.Login.UserName = config.Username;
            egBot.Login.Password = config.Password;
            if (String.IsNullOrEmpty(egBot.Login.UserName) || String.IsNullOrEmpty(egBot.Login.Password))
            {
                Console.WriteLine("Missing Username and Password.  Hit any key to exit.");
                Console.ReadKey();
                return;
            }
            foreach (ChatElement chat in config.ChatList)
                egBot.ChatList += "," + chat.Name;

            egBot.Zone.Connected += new ZoneConnectedHandle(zone_Connected);
            egBot.Zone.Disconnected += new ZoneDisconnectedHandle(zone_Disconnected);
            egBot.Login.LoginResponse += new LoginResponseHandle(Login_LoginResponse);
            egBot.Arena.ArenaInfoList += new ArenaInfoListHandle(arena_ArenaInfoList);
            egBot.Arena.PlayerEnter += new PlayerEnterHandle(arena_PlayerEnter);
            egBot.Messenger.MessageReceived += new MessageReceivedHandle(msger_MessageReceived);
            //-----------------------------------
            // -- Add your modules here! --
            egBot.AddModule<HelloWorld>(new HelloWorld());
            
            //---------------------------------------------
            egBot.Start();
            //-----------------------------------
            // -- Console/Control LOOP --
            //-----------------------------------
            String inputBuffer = String.Empty;
            bool running = true;
            Console.WriteLine("EGBot started!");
            while (running)
            {
                Console.Title = inputBuffer;
                ConsoleKeyInfo pressed = Console.ReadKey(true);
                switch (pressed.Key)
                {
                    case ConsoleKey.Enter:
                        if (inputBuffer.StartsWith("?exit"))
                        {
                            //Stop
                            egBot.Stop();
                            running = false;
                        }
                        else if (inputBuffer.StartsWith("?connect"))
                        {
                            egBot.Zone.Connect();
                        }
                        else if (inputBuffer.StartsWith("?disconnect"))
                        {
                            egBot.Zone.Disconnect();
                        }
                        else if (inputBuffer.StartsWith("?go"))
                        {
                            String arenaName = String.Empty;
                            if (inputBuffer.Length > 4)
                                arenaName = inputBuffer.Substring(4);

                            egBot.Arena.Go(ShipType.Spectate, arenaName);
                        }
                        else if (inputBuffer.StartsWith("?teams"))
                        {
                            StringBuilder teamInfoBuilder = new StringBuilder();
                            foreach (KeyValuePair<ushort, List<PlayerInfo>> item in egBot.Arena.TeamInfoMap)
                            {
                                teamInfoBuilder.Append(item.Key.ToString());
                                teamInfoBuilder.Append(": ");
                                foreach (PlayerInfo pid in (item.Value))
                                {
                                    teamInfoBuilder.Append(pid.Name);
                                    teamInfoBuilder.Append(", ");
                                }
                                teamInfoBuilder.Append(Environment.NewLine);
                            }
                            Console.WriteLine(teamInfoBuilder.ToString());
                        }
                        MessageType msgType = egBot.Messenger.Send(inputBuffer, SoundType.None);
                        Utilities.PrintToConsole(msgType, egBot.Arena.UserId, egBot.Login.UserName, inputBuffer);
                        inputBuffer = String.Empty;
                        break;
                    case ConsoleKey.Backspace:
                        if (inputBuffer.Length != 0)
                            inputBuffer = inputBuffer.Remove(inputBuffer.Length - 1);
                        break;
                    default:
                        inputBuffer += pressed.KeyChar;
                        break;
                }
            }
            Console.WriteLine("Engine stopped. Press any key to exit.");
            Console.ReadKey();
            return;
        }

        static void Login_LoginResponse(object sender, LoginResponseType loginResponseType)
        {
            Console.WriteLine(Enum.GetName(typeof(LoginResponseType), loginResponseType));
        }

        static void zone_Connected(object sender, bool canConnect)
        {
            Console.WriteLine("Connected to server.");
        }

        static void zone_Disconnected(object sender, bool lostConnection)
        {
            Console.WriteLine("Disconnected from server.");
        }

        static void arena_ArenaInfoList(object sender, ArenaInfoList arenaInfoList)
        {
            foreach (ArenaInfo ai in arenaInfoList)
            {
                Console.WriteLine(String.Format("{0} - {1}", ai.Name, ai.Population));
            }
            Console.WriteLine(String.Format("TotalPop: {0}", arenaInfoList.TotalPopulation));
        }

        static void arena_PlayerEnter(object sender, PlayerInfo playerInfo)
        {
            Console.WriteLine(String.Format("Player entered: {0}", playerInfo.Name));
        }

        static void msger_MessageReceived(object sender, MessageInfo msgInfo)
        {
            Utilities.PrintToConsole(msgInfo.MessageType, msgInfo.PlayerId, msgInfo.PlayerName, msgInfo.Message);
        }
    }
}
