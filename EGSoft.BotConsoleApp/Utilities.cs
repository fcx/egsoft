﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EGSoft.Fc.SubspaceModel;

namespace EGSoft.BotConsoleApp
{
    public class Utilities
    {
        public static void PrintToConsole(MessageType chatType, UInt16 pid, String name, String message)
        {
            switch (chatType)
            {
                case MessageType.Broadcast:
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;
                case MessageType.Chat:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case MessageType.Private:
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;
                case MessageType.PrivateRemote:
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;
                case MessageType.PublicMacro:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case MessageType.Public:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case MessageType.Server:
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    break;
                case MessageType.TeamBroadcast:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    break;
                case MessageType.Team:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case MessageType.Warning:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
            }
            Console.WriteLine("{3}::{2}:{0}> {1}", name, message, pid, chatType.ToString());
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
