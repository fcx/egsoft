﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;

namespace EGSoft.BotConsoleApp.Configuration
{
    public class BotConsoleAppConfig : ConfigurationSection
    {
        private static string sConfigurationSectionConst = "botConsoleAppConfig";

        public static BotConsoleAppConfig GetConfig()
        {
            return (BotConsoleAppConfig)System.Configuration.ConfigurationManager.
                GetSection(BotConsoleAppConfig.sConfigurationSectionConst) ??
                new BotConsoleAppConfig();
        }

        [ConfigurationProperty("username", IsRequired = true)]
        public string Username
        {
            get
            {
                return this["username"] as string;
            }
        }

        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get
            {
                return this["password"] as string;
            }
        }

        [ConfigurationProperty("zoneIP", IsRequired = true)]
        public string ZoneIP
        {
            get
            {
                return this["zoneIP"] as string;
            }
        }

        [ConfigurationProperty("zonePort", IsRequired = true)]
        public int ZonePort
        {
            get
            {
                return (int)this["zonePort"];
            }
        }

        [ConfigurationProperty("defaultArena", IsRequired = true)]
        public string DefaultArena
        {
            get
            {
                return this["defaultArena"] as string;
            }
        }

        [ConfigurationProperty("chatList")]
        public ChatElementCollection ChatList
        {
            get
            {
                return (ChatElementCollection)this["chatList"] ??
                    new ChatElementCollection();
            }
        }
    }

    public class ChatElementCollection : ConfigurationElementCollection
    {
        public ChatElement this[int index]
        {
            get
            {
                return base.BaseGet(index) as ChatElement;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ChatElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ChatElement)element).Name;
        }
    }

    public class ChatElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }
    }

}
