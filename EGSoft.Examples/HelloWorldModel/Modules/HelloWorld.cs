﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Text;

using EGSoft.Fc.EngineModel;
using EGSoft.Fc.SubspaceModel;
using EGSoft.Fc.SubspaceModel.Modules;

namespace EGSoft.Examples.HelloWorldModel.Modules
{
    //ModuleInfo Attributes: Information about this module
    [FcEngineModuleInfo(
        ModuleName = "HelloWorld",
        ModuleAlias = "HW",
        Author = "Fc",
        Version = "v1.0")]
    public class HelloWorld : IFcEngineModule
    {
        IArena arena;
        IMessenger messenger;

        public HelloWorld()
        {
            //Default constructor.
        }

        //Called when the engine loads the module
        //Return true if this module can be loaded successfully otherwise false.
        bool IFcEngineModule.OnLoad(IFcEngineLinker linker)
        {
            //Attach this modules that it needs to use
            arena = linker.AttachModule<IArena>();
            messenger = linker.AttachModule<IMessenger>();
            //Setup a function to listen to the Event that tells us when we have ArenaEntered
            arena.ArenaEntered += new ArenaEnteredHandle(arena_ArenaEntered);
            //Return true to indicate that this module has been loaded successfully.
            return true;
        }

        //Called when the engine unloads the module
        //Return true if this module can be unloads successfully otherwise false.
        bool IFcEngineModule.OnUnload(IFcEngineLinker linker)
        {
            //Remove the function that is listening on the Event ArenaEntered
            arena.ArenaEntered -= new ArenaEnteredHandle(arena_ArenaEntered);
            //Detach from the module you have attached to
            arena = linker.DetachModule<IArena>();
            messenger = linker.DetachModule<IMessenger>();
            //Return true to indicate that this module has been unloaded successfully.
            return true;
        }

        //This function gets invoked when we have entered an arena.
        void arena_ArenaEntered(object sender, ushort playerId)
        {
            //Tell the chat module to send out a public message with farting sound.
            messenger.SendPublic("Hello World!", SoundType.None);
        }
    }
}

