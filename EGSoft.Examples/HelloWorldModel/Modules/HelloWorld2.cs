﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Text;

using EGSoft.Fc.EngineModel;
using EGSoft.Fc.CoreModel;
using EGSoft.Fc.CoreModel.Modules;
using EGSoft.Fc.SubspaceModel;
using EGSoft.Fc.SubspaceModel.Modules;

namespace EGSoft.Examples.HelloWorldModel.Modules
{


    [FcEngineModuleInfo(
        ModuleName = "HelloWorld",
        ModuleAlias = "HW",
        Author = "Fc",
        Version = "v2.0")]
    public class HelloWorld2 : IFcEngineModule
    {
        IFcCore core;
        IArena arena;
        IMessenger messenger;

        IFcCommand sayCmd;
        FcCommandExecutedHandle sayCmdExecutedHandle;

        //public class SayCmd : FcCommand
        //{
        //    private static SayCmdParam parameters = new SayCmdParam(this.Paramters);

        //    public String Name { get { return "say"; } }
        //    public String Alias { get { return "s"; } }'
        //    public String Description { get { return "Says \"Hello, (your message)!\" or \"Bye, (your message)!\""; } }

        //    public IFcCommandParameters Parameters { get { return parameters; } }
        //    public class SayCmdParam : FcCommandParameter
        //    {
        //        public String Name { get { return "Message"; } }
        //        public String Alias { get { return "Mode"; } }
        //    }
        //}

        public HelloWorld2()
        {
            //Default constructor.
            sayCmd = new FcCommand("say", "s", "Says \"Hello, (your message)!\" or \"Bye, (your message)!\"");
            sayCmd.Parameters.Add(new FcCommandParameter<string>("message", "msg", "Your output message."));
            sayCmd.Parameters.Add(new FcCommandParameter<int>("mode", "m", "0 for Hello or 1 for Bye", 0));
            sayCmdExecutedHandle = new FcCommandExecutedHandle(sayCmd_Executed);
        }

        bool IFcEngineModule.OnLoad(IFcEngineLinker linker)
        {
            core = linker.AttachModule<IFcCore>();
            core.Commands.Register(sayCmd).Executed += sayCmdExecutedHandle;

            arena = linker.AttachModule<IArena>();
            arena.ArenaEntered += new ArenaEnteredHandle(arena_ArenaEntered);

            messenger = linker.AttachModule<IMessenger>();
            return true;
        }

        bool IFcEngineModule.OnUnload(IFcEngineLinker linker)
        {
            core.Commands.Register(sayCmd).Executed -= sayCmdExecutedHandle;
            core = linker.DetachModule<IFcCore>();

            arena.ArenaEntered -= new ArenaEnteredHandle(arena_ArenaEntered);
            arena = linker.DetachModule<IArena>();

            messenger = linker.DetachModule<IMessenger>();
            return true;
        }

        void sayCmd_Executed(object sender, IFcCommandContext context)
        {
            String message = context.Arguments.Get<string>("message");
            int mode = context.Arguments.Get<int>("mode");
            if (mode == 0)
                messenger.SendPublic("Hello World! " + message, SoundType.None);
            else if (mode == 1)
                messenger.SendPublic("Bye World! " + message, SoundType.None);
            else
                context.Reply("Invalid mode:" + sayCmd.Parameters.Get("mode").Description);
        }

        void arena_ArenaEntered(object sender, ushort playerId)
        {
            //messenger.SendPublic("Hello World!", SoundType.Fart);
        }
    }
}

